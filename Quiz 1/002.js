console.log(' ')
console.log('A. Descending Ten')
console.log('==================================')

function DescendingTen(angka) {
  // Tulis code kamu di sini
  if(angka !== undefined){

    var x = []
    var y = angka-10
    for(angka; angka > y ; angka-- ){
      x.push(angka)
    }
    return x.join(" ")
  }

  else 
    return -1
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1


console.log(' ')
console.log('B. Ascending Ten')
console.log('==================================')

function AscendingTen(number) {
  // Tulis code kamu di sini
  if(number !== undefined){

    var x = []
    var y = number+10
    for(number; number < y ; number++ ){
      x.push(number)
    }
    return x.join(" ")
  }

  else 
    return -1
}

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1


console.log(' ')
console.log('C. Conditional Ascending Descending')
console.log('==================================')

function ConditionalAscDesc(reference, check) {
  // Tulis code kamu di sini
  if(reference && check !== undefined){

    var x = []
    if(check % 2 == 0){
      var i = reference-10
      for(reference; reference > i ; reference-- ){
        x.push(reference)
      }
      return x.join(" ")
    }

    else{
      var i = reference+10
      for(reference; reference < i ; reference++ ){
        x.push(reference)
      }
      return x.join(" ")
    }
  }

  else 
    return -1
}

console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1


console.log(' ')
console.log('D. Papan Ular Tangga')
console.log('==================================')

function ularTangga() {
  // Tulis code kamu di sini
  for(let i = 10; i>1; i--){

    var x= []
    var y= []
    let number = i*10
    if(i%2 == 0){
      arrayUT = number-10
      for(number; number > arrayUT; number--){
        y.push(number)
      }
      x.push(y)
    }
    
    else{
      arrayUT = number-10
      for(arrayUT; arrayUT <= number; arrayUT++){
        y.push(arrayUT)
      }
    }
    console.log(y.join(" "))
  }
}

console.log(ularTangga())