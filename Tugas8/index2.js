console.log('No. 2 (Promise Baca Buku)')
console.log('======================')

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function read (i, time){
    if(i > books.length-1){
        return ;

    }
    else{
        readBooksPromise(time,books[i])
        .then(function(times){
            return i + read(i + 1,times)
        
        })
        .catch(function(error){
            return error

        })
    }
}

read(0,10000)
