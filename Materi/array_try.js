function sum(startNum, finishNum, step = 1) {
    var total=0
    if(startNum < finishNum) {
      for(startNum; startNum <= finishNum; startNum+=step){
        total += startNum
      }
      return total
    }
    else {
      for(startNum; startNum >= finishNum; startNum-=step){
        total += startNum
      }
      return total
    }
  }
  console.log(sum(1,10)) // 55
  console.log(sum(5, 50, 2)) // 621
  console.log(sum(15,10)) // 75
  console.log(sum(20, 10, 2) // 90
  console.log(sum(1)) // 1
  console.log(sum()) // 0 