console.log('Tugas 4 – Array')
console.log(' ')
console.log('No. 1')
console.log('======================')


function range(startNum, finishNum){
  
    var arr = []
    if(startNum && finishNum !== undefined && startNum <finishNum){

      for(startNum; startNum<=finishNum; startNum++){

        arr.push(startNum)
      }
      return arr
    }else if(startNum>finishNum){

      for(startNum; startNum>=finishNum; startNum--){

        arr.push(startNum)
      }
      return arr
    }
    else return -1
  }
  
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log('\n')
  

console.log(' ')
console.log('No. 2')
console.log('======================')


function rangeWithStep(startNum, finishNum, step) {

    var arr = []
    if(startNum < finishNum) {

      for(startNum; startNum < finishNum; startNum+=step){
        arr.push(startNum)
      }
      return(arr)
    }
    else { for(startNum; startNum > finishNum; startNum-=step){

        arr.push(startNum)
      }
      return(arr)
    }
  }
  
  console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
  console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
  console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
  console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
  console.log('\n')  

  console.log(' ')
  console.log('No. 3')
  console.log('======================')
  

function sum(startNum, finishNum, step = 1) {

    var total=0
    if(startNum < finishNum) {

      for(startNum; startNum <= finishNum; startNum+=step){
        total += startNum
      }
      return total
    }
    else { for(startNum; startNum >= finishNum; startNum-=step){

        total += startNum
      }
      return total
    }
  }
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0  

console.log(' ')
console.log('No. 4')
console.log('======================')


function dataHandling(input){

    for(let a=0; a<input.length; a++){
      console.log(`Nomor ID: ${input[a][0]}`)
      console.log(`Nama Lengkap: ${input[a][1]}`)
      console.log(`TTL: ${input[a][2]} ${input[a][3]} `)
      console.log(`Hobi: ${input[a][4]}`)
      console.log('\n')
    }
  }
  
  var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ] 
  
  dataHandling(input)  

  console.log(' ')
  console.log('No. 5')
  console.log('======================')
  

function balikKata(str){

    let arr= ""
    for(let i = str.length; i>= 0; i-- ){
      arr+= str.charAt(i)
    }
    console.log(arr)
  }
  
balikKata("Kasur Rusak") // kasuR rusaK
balikKata("SanberCode") // edoCrebnaS
balikKata("Haji Ijah") // hajI ijaH
balikKata("racecar") // racecar
balikKata("I am Sanbers") // srebnaS ma I 
  

console.log(' ')
console.log('No. 6')
console.log('======================')


function dataHandling2(input){

  input.splice(2,1,"Provinsi Bandar Lampung")
  input.splice(4,1,"Pria", "SMA Internasional Metro")
  console.log(input)

  var bulan = input[3]
  var bulanH=bulan.split('/')
  switch(bulanH[1]){

    case '01':console.log('Januari'); break;
    case '02':console.log('Februari'); break;
    case '03':console.log('Maret'); break;
    case '04':console.log('April'); break;
    case '05':console.log("Mei"); break;
    case '06':console.log("Juni"); break;
    case '07':console.log("Juli"); break;
    case '08':console.log("Agustus"); break;
    case '09':console.log("September"); break;
    case '10':console.log("Oktober"); break;
    case '11':console.log("November"); break;
    case '12':console.log("Desember"); break;
  }
  console.log(bulanH.sort(function(a, b){return b - a}))
  console.log(bulan.split("/").join("-"))
  console.log(input[1].slice(0,15))
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
